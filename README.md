Please, use this repository as a base for your training camp. The idea is to fork it, so everyone use the same folder structure for the exercises.

Index:
------

1.  [*Objective*](#objective)

2.  [*Who should be attending*](#who-should-attend)

3.  [*Duration*](#duration)

4.  [*Technical assistance*](#technical-assistance)

5.  [*Performance measurement*](#performance-measurement)

6.  [*Handling advanced TechOps*](#handling-advanced-techops)

7.  [*Materials*](#materials)

8.  [*Bootcamp schedule*](#bootcamp-schedule)

9.  Learning:

    1.  [*Pre-bootcamp: Github basic usage*](#pre-bootcamp-github-basic-usage)

    2.  [*Week 1: DevOps culture and methodology*](#week-1-devops-culture-and-methodology)

    3.  [*Week 2: Common DevOps tools of the trade*](#week-2-common-devops-tools-of-the-trade)

    4.  [*Week 3: CI/CD tool of excellence: Jenkins*](#week-3-cicd-tool-of-excellence-jenkins)

    5.  [*Week 4: Putting it all together, final exercise.*](#week-4-putting-it-all-together-final-exercise.)

**Objective**
-------------

This course teaches the basics of the DevOps world, and its main concepts and tools.

**Who should attend**
---------------------

You should have some experience in Systems Administration (Linux and/or Windows), and at least know or have heard the basic concepts of virtualization, cloud computing, and code versioning.

**Duration**
------------

Four weeks total (20 days). Previous to this, attendees are expected to read the pre-bootcamp Github course at their own pace.

**Technical assistance**
------------------------

You can contact other Bootcamp participants or any available tutor if you need technical assistance. We will create one chat for boot camp members only, and another one for boot camp members and tutors when boot camp starts.

**Performance Measurement**
---------------------------

1.  Code review after each practice.
    --------------------------------

2.  Checkpoint completion after Learning stage with your assigned tutor.
    --------------------------------------------------------------------

**Handling advanced TechOps**
-----------------------------

People that move faster than average can complete the exercises at their own pace and send them earlier.

**Materials**
-------------

You will need a headset, for hangouts calls. Also, you’ll need to fork this repo to use as a base to host the project code and documentation required.

**Bootcamp schedule**
---------------------

This bootcamp is divided into three weeks, dedicated for mostly reading the material indicated. There is an exercise at the end of each week, which is required to be sent by the end of each week’s Friday. If you can’t reach the deadline, contact your tutor and request an extension, shouldn’t be any problems.

***Pre-bootcamp: Github basic usage***
--------------------------------------

Previous to engaging our DevOps bootcamp, you need to understand the basics of Github, the most widely used [*version control software*](https://en.wikipedia.org/wiki/Version_control). It’s an integral part of every Information Systems professional team.

-   [*Online Git course*](https://try.github.io/levels/1/challenges/1)

-   [*Github specific course*](https://guides.github.com/activities/hello-world/)

-   [*Git course notes*](https://github.com/globant-ui-rosario/git-course) - what to focus on, to get ahead on this bootcamp.

***Week 1: DevOps culture and methodology***
--------------------------------------------

The first week is dedicated to understand DevOps culture and the first big team it interacts with: Developers and their tools and working methods.

-   [*What is 'DevOps' ?*](http://theagileadmin.com/what-is-devops/) [*Another definition.*](https://en.wikipedia.org/wiki/DevOps) [*"Official" website.*](http://devops.com/)

-   Methodology of work: [*Agile*](https://en.wikipedia.org/wiki/Agile_software_development)

-   Enter the [*'Development Environments'*](http://dltj.org/article/software-development-practice/): concepts and best practices.

-   [*Continuous Integration*](https://en.wikipedia.org/wiki/Continuous_integration) and [*Continuous Delivery*](https://en.wikipedia.org/wiki/Continuous_delivery). [*What is the difference, if any, between them?*](http://blog.nwcadence.com/continuousintegration-continuousdelivery/)

-   [*The Continuous Delivery pipeline - definition and concepts*](http://devops.com/2014/07/29/continuous-delivery-pipeline/)

-   [*Continuous Delivery pipeline, in depth*](http://www.informit.com/articles/article.aspx?p=1621865)

-   Recommended view: [*Continuous Delivery by Martin Fowler*](https://www.youtube.com/watch?v=aoMfbgF2D_4)

-   Recommended read: [*Key Topics in the Release Management Process*](http://electric-cloud.com/wiki/display/releasemanagement/Release+Management+Process#ReleaseManagementProcess-KeyTopicsintheReleaseManagementProcess)

**Exercise:**

****Note****: these are real facts from a real client, which name obviously won’t be disclosed. A solution was found using above’s theory and practices.

A company that has recently started their own development team for internal projects. After a while, they contact you because their development and release cycle (They work using Agile methodology), is a “mess” - using their words.

After a quick survey and information gathering meeting, these facts could be noticed:

-   All 25 developers work on three different projects. The company has an SVN server, but it’s not always updated since some of them work strictly off their own workstations, and when it’s time to build the release they meet to see who has the latest code, and they merge it on the run and build locally.

-   The application was tested ***locally*** in each developer’s workstation and working out the errors. After a consensus that the release was ok to deploy, the final release package was manually built.

-   The projects were two PHP websites and one Java backend. When the time came for releasing to Production, the process was: backing up what was currently in production in a tar.gz file and keep it for a couple of weeks just in case, and overwriting with the new package.

Since they realized they needed to hire more developers because they were going to need to confront more projects soon, they requested us to help them organize their process because:

-   Project Manager says:

    -   Process to reach consensus on finding latest changes and build the release took too long; it’s time developers could use for solving problems or start coding new features.

    -   Testing also took too long, and never seemed enough since lots of bugs were found after each release. He would like his team to focus on coding and not spend time on finding errors, just solve them.

-   Product Owner says:

    -   Going forward, they realized they would need to keep a history of the builds for company’s standards compliance.

    -   They would like to give the end users the possibility to have user acceptance testing on their app before it’s released.

I’m asking you to write a document, on what you would propose by applying the theory and practices you read on this week. Feel free to explain as much as you want, using general or specific tools and/or processes. Take into consideration that the solution has to include the momento each developer writes a change, to the moment any application is deployed to production servers.
Include graphs, pictures, anything you think will help to expose your view on the matter :)

The data we gave you may look incomplete (it actually is), make all the assumptions you want (and please explain them) in the process you would take to improve this company’s release process.

***Week 2: Common DevOps tools of the trade***
----------------------------------------------

Now, we’re going to learn the most common tools a DevOps engineer uses in day-to-day tasks.

-   You’ve seen the online course, but in day-to-day work, we normally use the following features:

    -   [*Git*](https://git-scm.com/) - [*Official documentation*](https://git-scm.com/doc).

    -   [*Learn git branching concepts online*](http://learngitbranching.js.org/)

    -   [*What is Github?*](https://en.wikipedia.org/wiki/GitHub) - [*Official site*](https://github.com/)

    -   [*Gitflow*](https://guides.github.com/introduction/flow/): workflow used (sometimes a bit different) in most companies.

    -   Recommended view: [*Git tutorial, series of videos*](https://www.youtube.com/playlist?list=PLjQo0sojbbxVHcVN4h9DMu6U6spKk21uP)

-   Configuration management:

    -   [*Definition, overview and types of CM*](http://electric-cloud.com/wiki/display/releasemanagement/Configuration+Management+Tools)

    -   [*Best options of CM tools to choose from*](http://www.tomsitpro.com/articles/configuration-management-tools,2-920.html)

    -   [*Chef tutorial*](https://learn.chef.io/)

    -   [*How does Puppet work? + Tutorial*](http://www.slashroot.in/puppet-tutorial-how-does-puppet-work)

-   Virtualization automation: [*Vagrant*](https://www.vagrantup.com/)

    -   [*Getting started: installation and tutorial*](https://www.vagrantup.com/docs/getting-started/)

-   Containers:

    -   [*What is containerization?*](http://www.webopedia.com/TERM/C/containerization.html)

    -   [*Do I choose Vms or containers for my projects?*](http://www.itworld.com/article/2915530/virtualization/containers-vs-virtual-machines-how-to-tell-which-is-the-right-choice-for-your-enterprise.html)

    -   [*Enter Docker:*](https://www.docker.com/) the most used container technology, today.

    -   [*Official documentation*](https://docs.docker.com/)

    -   [*Getting started*](https://docs.docker.com/engine/getstarted/)

    -   Recommended view: [*Docker tutorial, series of videos*](https://www.youtube.com/playlist?list=PLjQo0sojbbxViGEbI_87SPXpb3neuVqDL)

**Exercise:**

Create a VM or series of VMs with Vagrant; or container(s) with Docker. Choose Chef or Puppet, to manage their configurations.

You can choose whatever you like for your machines or containers, the same for the packages or software you’d like to manage in them.

Then, create an account on Github (if you’re not already on it), and create a repository, where you will store every configuration file:

-   Vagrant / Docker files

-   Chef / Puppet recipes

***Notes:***

-   Try to be original, and make everything yourself. Don’t be afraid to propose or make mistakes.

-   Every change you make to your files as you create them, try to version them using Github, so the history remains there.

-   When you finish, send me your Github repo link. I will test your recipes and files.

***Week 3: CI/CD tool of excellence: Jenkins***
-----------------------------------------------

Now, the tool that brings it all together: [*Jenkins*](https://jenkins.io/index.html).

-   [*Official documentation*](https://jenkins.io/doc/)

-   [*Jenkins complete tutorial*](http://www.tutorialspoint.com/jenkins/)

-   [*Jenkins pipeline as code*](https://jenkins.io/solutions/pipeline/): Starting version 2.0, manual plans creation is a thing of the past. With this, even plan creation can be automated and treated as individual CI plans!

-   [*Jenkins pipeline as code, pre 2.0*](https://www.slalom.com/thinking/automatically-generating-jenkins-jobs)

-   Recommended read: [*Building a Continuous Integration Pipeline with Docker*](https://www.docker.com/sites/default/files/RA_CI%20with%20Docker_08.25.2015.pdf) (PDF)

***Exercise:***

-   Install Jenkins in a VM or container, and set it up with:

    -   Github plugin

    -   Secure Jenkins so it doesn’t let anyone get in without identification.

    -   Create a script (language of your choice) that lists the contents of a directory passed at run time as a parameter, and upload it to Github.

    -   Use the previous script in a Jenkins plan that uses it, and passes the directory as a [*parameterized build*](https://wiki.jenkins-ci.org/display/JENKINS/Parameterized+Build) / job execution.

    -   When finished, leave the Jenkins config files and the resource you chose to install it in (VM, container) in the bootcamp github repo.

***Week 4: Putting it all together, final exercise.***
------------------------------------------------------

We want you to create a CI project in Jenkins, following these suggestions:

-   Find a project that you like, in the language you like. For example:

    -   [*Selection of Java projects*](http://freesourcecode.net/javaprojects#.V3F677jhDDc)

    -   [*Selection of .Net projects (VB :Net)*](http://freesourcecode.net/vbdotnetprojects#.V3F8K7jhDDc)

    -   [*Selections of C projects*](http://freesourcecode.net/cprojects#.V3F8TrjhDDc)

-   Build the project, first manually. That will give you the command you need for automating it with Jenkins. If you find it too difficult, you can search for a project that doesn’t need building, only packaging. Example: [*PHP projects*](http://www.onlinefreeprojectdownload.com/phpprojects.html).

-   Once you managed to build and//or package your project, we’ll ask you to:

    -   Maintain a Github code repository

    -   Automate the build

    -   Every commit to the baseline (code repo) must be built

    -   If the build is successful, it should be automatically deployed to an environment created by you, be it in a VM or container built at Jenkins build time. You can integrate them both with [*this plugin*](https://wiki.jenkins-ci.org/display/JENKINS/Docker+build+step+plugin).

-   Keep all of your configuration files in a separate repository (Jenkins configs, Docker, Vagrant, Chef / Puppet)

-   For extra consideration, investigate and let me know how and in which stage you would automate testing for the project you chose.

We will try and test your configurations, or ask you to show it to us via shared screen. The exercise will be a successful one when with each push to the repository, Jenkins fires up a new build, and installs it to the test machine / container you created.

-   
